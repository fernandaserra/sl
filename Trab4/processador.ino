/*
  Correspondência entre pinos digitais e entradas:
  A0  A1    2   3   4   5   6         7    8    9    10   11   12   13
  M   Cin  S0  S1  S2  S3  MemtoReg   rA1  rA0  rB1  rB0   wr0  wr1  clk
                                      im0  im1  im2  im3
*/

void setup() {
  for (byte i = 2; i <= 13; i++) {
    pinMode(i, OUTPUT);
    digitalWrite(i, LOW);
  }
  pinMode(A0,OUTPUT);
    digitalWrite(A0, LOW);
  pinMode(A1,OUTPUT);
    digitalWrite(A1, LOW);
  // inicializa o serial
  Serial.begin(9600);
  Serial.println("Formato do comando (Operação da ula): {M, Cin, S0, S1, S2, S3, 0, A0, A1, B0, B1, wr0, wr1}");
  Serial.println("Formato do comando (Guardar imediato): {X, X, X, X, X, X, 1, im0, im1, im2, im3, wr0, wr1}");
  Serial.println("Ambos sem espaços, virgulas e chaves.");
  Serial.println("Ex: 0101010101110");
}

byte valida(String comando){
  // a string deve ter tamanho 13 e so pode conter 0s e 1s
  if(comando.length() != 13) 
    return 0;
  for(int i=0; i<13; i++) 
    if(comando[i]!='0' && comando[i]!='1') 
      return 0;
  return 1;
}

void updateClock(){
  digitalWrite(13, LOW);
  delay(50);
  digitalWrite(13, HIGH);
}

void loop() {
    Serial.println("Digite o comando: ");
    while(Serial.available() == 0);
      String comando = Serial.readString();
      Serial.println(comando);
      if(!valida(comando)) 
        Serial.println("A string deve ter tamanho 13 e so pode conter 0s e 1s.");
      else{
        digitalWrite(A0, comando[1] - '0');
        digitalWrite(A1, comando[0] - '0');
        for(byte i=2; i<13; i++) {
          digitalWrite(i, comando[i] - '0');
        }          
        updateClock();
        Serial.println("Comando executado.");
      }
}
