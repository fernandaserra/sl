#define TEMP 1000

void setup() {
  pinMode(0, OUTPUT);
  pinMode(1, OUTPUT);

}

void loop() {
  digitalWrite(0, HIGH);
  digitalWrite(1, LOW);
  delay(TEMP);

}
